import { tap, expect } from '@pushrocks/tapbundle';
import { Qenv } from '@pushrocks/qenv';
import * as smartmongo from '@pushrocks/smartmongo';
import { smartunique } from '../ts/smartdata.plugins.js';

const testQenv = new Qenv(process.cwd(), process.cwd() + '/.nogit/');

console.log(process.memoryUsage());

// the tested module
import * as smartdata from '../ts/index.js';

// =======================================
// Connecting to the database server
// =======================================

let smartmongoInstance: smartmongo.SmartMongo;
let testDb: smartdata.SmartdataDb;

const totalCars = 2000;

tap.test('should create a testinstance as database', async () => {
  smartmongoInstance = await smartmongo.SmartMongo.createAndStart();
  testDb = new smartdata.SmartdataDb(await smartmongoInstance.getMongoDescriptor());
  await testDb.init();
});

tap.skip.test('should connect to atlas', async (tools) => {
  const databaseName = `test-smartdata-${smartunique.shortId()}`;
  testDb = new smartdata.SmartdataDb({
    mongoDbUrl: testQenv.getEnvVarOnDemand('MONGO_URL'),
    mongoDbName: databaseName,
  });
  await testDb.init();
});

@smartdata.Collection(() => testDb)
class House extends smartdata.SmartDataDbDoc<House, House> {
  @smartdata.unI()
  public id: string = smartunique.shortId();

  @smartdata.svDb()
  public data = {
    id: smartunique.shortId(),
    hello: 'hello',
  };
}

tap.test('should watch a collection', async (toolsArg) => {
  const done = toolsArg.defer();
  const watcher = await House.watch({});
  watcher.changeSubject.subscribe(async (houseArg) => {
    console.log('hey there, we observed a house');
    await watcher.close();
    done.resolve();
  });
  const newHouse = new House();
  await newHouse.save();
  console.log('saved a house');
  await done.promise;
});

// =======================================
// close the database connection
// =======================================
tap.test('close', async () => {
  await testDb.mongoDb.dropDatabase();
  await testDb.close();
  if (smartmongoInstance) {
    await smartmongoInstance.stop();
  }
});

tap.start({ throwOnError: true });

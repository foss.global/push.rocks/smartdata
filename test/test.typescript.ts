import { tap, expect } from '@pushrocks/tapbundle';
import { Qenv } from '@pushrocks/qenv';
import * as smartmongo from '@pushrocks/smartmongo';
import { smartunique } from '../ts/smartdata.plugins.js';

const testQenv = new Qenv(process.cwd(), process.cwd() + '/.nogit/');

console.log(process.memoryUsage());

// the tested module
import * as smartdata from '../ts/index.js';

// =======================================
// Connecting to the database server
// =======================================

let smartmongoInstance: smartmongo.SmartMongo;
let testDb: smartdata.SmartdataDb;

const totalCars = 2000;

tap.test('should create a testinstance as database', async () => {
  smartmongoInstance = await smartmongo.SmartMongo.createAndStart();
  testDb = new smartdata.SmartdataDb(await smartmongoInstance.getMongoDescriptor());
  await testDb.init();
});

tap.skip.test('should connect to atlas', async (tools) => {
  const databaseName = `test-smartdata-${smartunique.shortId()}`;
  testDb = new smartdata.SmartdataDb({
    mongoDbUrl: testQenv.getEnvVarOnDemand('MONGO_URL'),
    mongoDbName: databaseName,
  });
  await testDb.init();
});

// =======================================
// The actual tests
// =======================================

// ------
// Collections
// ------
@smartdata.Manager()
class Car extends smartdata.SmartDataDbDoc<Car, Car> {
  @smartdata.unI()
  public index: string = smartunique.shortId();

  @smartdata.svDb()
  public color: string;

  @smartdata.svDb()
  public brand: string;

  @smartdata.svDb()
  deepData = {
    sodeep: 'yes',
  };

  constructor(colorArg: string, brandArg: string) {
    super();
    this.color = colorArg;
    this.brand = brandArg;
  }
}

const createCarClass = (dbArg: smartdata.SmartdataDb) => {
  smartdata.setDefaultManagerForDoc({ db: dbArg }, Car);
  return Car;
};

tap.test('should produce a car', async () => {
  const CarClass = createCarClass(testDb);
  const carInstance = new CarClass('red', 'Mercedes');
  await carInstance.save();
});

tap.test('should get a car', async () => {
  const car = Car.getInstance({
    color: 'red',
  });
});

// =======================================
// close the database connection
// =======================================
tap.test('close', async () => {
  await testDb.mongoDb.dropDatabase();
  await testDb.close();
  if (smartmongoInstance) {
    await smartmongoInstance.stop();
  }
});

tap.start({ throwOnError: true });
